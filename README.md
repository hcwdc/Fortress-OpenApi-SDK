# 项目说明

本项目为castle-fortress-basic的统一对外开放API接口调用SDK。指在方便开发人员直接将本程序嵌入内部进行简单修改即可使用。


> 所有的接入必须需要生成SecretId和SecretKey  
> 对外开放的接口访问路径统一为：/openapi/***

## 简要描述

### 请求签名

对于每一次HTTP或者HTTPS协议请求，我们会根据访问中的签名信息验证访问请求者身份。

SecretId和SecretKey是加密签名字符串和服务器端验证签名字符串的密钥，必须严格保密谨防泄露。


## 秘钥对生成签名的规则

1. 构造签名参数集合,除接口所需所有参数外,必传C-Date以及C-Secret(C-Date为header里的时间，C-Secret为用户的SecretId)

1. 将所有参数集合去除参数值为null和空的数据

1. 将所有参数key值按照ASCII码升序排列

1. 拼接签名字符串

1. 签名字符串进行HmacSHA1摘要 /Base64加密

### 1、构造签名参数集合

***

除接口所需所有参数外,必传C-Date以及C-Secret

```text
java.util.Map<String,Object> map=new java.util.HashMap<>();
	map.put("cDate","PP********");
	map.put("cSecretId","12QWgV-******-******-5odKA3");
	//请求接口所需的其他参数项
	map.put("……","……");……
```

### 2、 将所有参数集合去除参数值为null和空的数据

```text
//构造签名参数集合
Map<String,Object> signParamMap=new HashMap<>();
	if(map!=null && !map.isEmpty()){
		for(String key:map.keySet()){
		// 筛选 非null且非空的参数参与签名
			if(map.get(key)!=null && Strings.isNotEmpty(map.get(key).toString())){
				signParamMap.put(key,map.get(key));
			}
	}
}
```

### 3、 将所有参数key值按照ASCII码升序排列

```text
TreeMap<String,Object> treeMap=new TreeMap();
	for(String key:signParamMap.keySet()){
		treeMap.put(key,signParamMap.get(key));
	}
```

### 4、拼接生成待签名字符串

```text
//拼接 将排序后的参数与其值，组合成“参数=参数值”的格式，并且把这些参数用&字符连接来，此时生成的字符串为待签名字符串 最后一个&符要去掉
StringBuilder sb=new StringBuilder();
	for(String key:treeMap.keySet()){
		sb.append(key+"="+treeMap.get(key)+"&");
	}
	sb=new StringBuilder(sb.toString().substring(0,sb.lastIndexOf("&")));
	String signString = sb.toString();
```

### 5、签名字符串进行HmacSHA1摘要 /Base64加密

```text
//HmacSHA1摘要
byte[] bytes=signString.getBytes(StandardCharsets.UTF_8);
HMac mac = new HMac(HmacAlgorithm.HmacSHA1, 
//此处的secretKey为secretid对应的key
secretKey.getBytes(StandardCharsets.UTF_8));
String macHex1 = mac.digestHex(bytes);
//Base64加密
String sign = Base64.encode(macHex1);
return sign;
```

最后的sign为生成的签名
